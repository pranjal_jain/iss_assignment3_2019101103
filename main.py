import pygame
import random
import math
import time
from config import *

#initialise pygame
pygame.init()

pygame.display.set_icon(icon)

#collision
def isCollision(X1,Y1,X2,Y2):
	if((X2<=(X1+24)<=(X2+64) and Y2 <= Y1 <= (Y2 +64)) or (X2 <= (X1+24) <= (X2+64) and Y2 <= (Y1+24) <= (Y2 +64)) or (X2 <= (X1+24) <= (X2+64) and Y2 <= Y1 <= (Y2 +64)) or (X2 <= X1 <= (X2+64) and Y2 <= (Y1+24) <= (Y2 +64))):

		return True
	else:
		return False

#game over of player1
def end1(p):
	global playerImg 
	global player_status
	global player1_score
	global check1
	global check3
	global player1_time
	global start_time
	player1_time += (time.time() - start_time)
	start_time = time.time()
	p.X_cood=400
	p.Y_cood=0
	player_status+=1
	check1=0
	check3=0
	player1_score += p.score
	p.score=0
	playerImg = pygame.image.load("player2.png")

#game over of player2
def end2(p):
	global playerImg
	global player_status
	global player2_score
	global check2
	global check4
	global player2_time
	global start_time
	player2_time += (time.time() - start_time)
	start_time = time.time()
	p.X_cood=400
	p.Y_cood=600

	player_status+=1

	if player_status == 4:
		running=False
		hold2=True

	check2=0
	check4=0
	player2_score+=p.score
	p.score=0
	playerImg = pygame.image.load("player.png")

font = pygame.font.Font("freesansbold.ttf",32)

p1 = Player()

#start screen
while hold:
	for event in pygame.event.get():
		if event.type==pygame.QUIT:
			hold=False
		text = font.render("PRESS ANY KEY TO START", True, (255,255,255))
		screen.blit(text, (200,250))
		if event.type == pygame.KEYDOWN:
			running=True
			hold=False

	pygame.display.update()

#main screen
while running:

	screen.fill((51,153,255))

	slab_y=0
	for i in range (0,6):
		pygame.draw.rect(screen,(153,76,0),(0,slab_y,800,30))
		slab_y += 100

	pygame.draw.rect(screen,(153,76,0),(0,slab_y,800,30))

	for i in range(len(blocks)):
		screen.blit(fixed_obstacle1Img,(blocks[i].X_cood,blocks[i].Y_cood))

	screen.blit(playerImg,(p1.X_cood,p1.Y_cood))

	for i in range(len(ships)):
		ships[i].move()
		screen.blit(shipImg,(ships[i].X_cood, ships[i].Y_cood))

	msg = font.render("Player1_Score: " + str(player1_score) ,True ,(255 , 255 , 255))
	screen.blit(msg,(0,0))

	msg = font.render("Player2_Score: " + str(player2_score) ,True ,(255 , 255 , 255))
	screen.blit(msg,(500,600))

	if check1==6:
		end1(p1)

	if check2==6:
		end2(p1)

	for i in range(len(ships)):
		if isCollision(p1.X_cood, p1.Y_cood, ships[i].X_cood, ships[i].Y_cood):

			if player_status==4:
				player2_time += (time.time() - start_time)
				start_time = time.time()
				player2_score+=p1.score		
				running=False
				hold2=True

			elif player_status%2 == 1:
				end1(p1)
			elif player_status%2 == 0:
				if player_status==2:
					for j in range(len(ships)):
						ships[j].change_speed()
				end2(p1)

	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			running=False

	key=pygame.key.get_pressed()
	p1.move(key)
	screen.blit(playerImg,(p1.X_cood,p1.Y_cood))

	for i in range(len(ships)):
		if isCollision(p1.X_cood, p1.Y_cood, ships[i].X_cood, ships[i].Y_cood) :
			if player_status==4:
				player2_time += (time.time() - start_time)
				start_time = time.time()	
				player2_score+=p1.score
				running=False
				hold2=True			
			elif player_status%2 ==1:
				end1(p1)
			elif player_status%2 ==0:
				if player_status==2:
					for j in range(len(ships)):
						ships[j].change_speed()
				end2(p1)

	for i in range(len(blocks)):
		if isCollision(p1.X_cood, p1.Y_cood, blocks[i].X_cood, blocks[i].Y_cood) :
			if player_status==4:
				player2_time += (time.time() - start_time)
				start_time = time.time()
				player2_score+=p1.score
				running=False
				hold2=True
			elif player_status%2 ==1:
				end1(p1)
			elif player_status%2==0:
				if player_status==2:
					for j in range(len(ships)):
						ships[j].change_speed()
				end2(p1)	

		show = font.render("Current_Score: " + str(p1.score) ,True ,(255 , 255 , 255))
		screen.blit(show,(500,0))

	pygame.display.update()

#end screen
while hold2:
	screen.fill((255,0,0))
	text = font.render("GAME OVER", True, (0,0,0))
	screen.blit(text,(350,250))

	if player1_score < player2_score:

		text2 = font.render("WINNER:PLAYER 2",True,(0,0,0))
		screen.blit(text2,(300, 450))

	elif player1_score > player2_score:

		text2 = font.render("WINNER:PLAYER 1",True,(0,0,0))
		screen.blit(text2,(300, 450))

	else:
		if player1_time < player2_time:
			text2 = font.render("WINNER:PLAYER 1",True,(0,0,0))
			screen.blit(text2,(300, 450))

		elif player1_time > player2_time:
			text2 = font.render("WINNER:PLAYER 1",True,(0,0,0))
			screen.blit(text2,(300, 450))			

	for event in pygame.event.get():
		if event.type==pygame.QUIT:
			hold2=False

	pygame.display.update()
